<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Fencer;

class FencerControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    public $client;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    public function testShowFencers()
    {
        $crawler = $this->client->request('GET', 'api-fencing/fencers');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }

    public function testShowOneFencer()
    {
        $crawler = $this->client->request('GET', 'api-fencing/fencers/1');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }
}
