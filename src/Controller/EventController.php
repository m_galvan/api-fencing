<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\EventRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Event;

/**
 * @Route("api-fencing/events")
 */
class EventController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    public function __construct(SerializerInterface $serializerInterface)
    {
        $this->serializer = $serializerInterface;
    }

    /**
     * @Route(methods="GET")
     */
    public function getAllEvents(EventRepository $eventRepo)
    {
        $event = $eventRepo->findAll();
        $json = $this->serializer->serialize($event, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/{event}", methods="GET")
     */
    public function getOneEvent(Event $event)
    {
        return new JsonResponse($this->serializer->serialize($event, 'json'), 200, [], true);
    }
}
